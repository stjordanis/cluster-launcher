# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.1.0"
  constraints = ">= 2.0.0"
  hashes = [
    "h1:/OpJKWupvFd8WJX1mTt8vi01pP7dkA6e//4l4C3TExE=",
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "h1:KtUCltnScfZbcvpE9wPH+a0e7KgMX4w7y8RSxu5J/NQ=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "1.14.3"
  constraints = ">= 1.14.0"
  hashes = [
    "h1:GrRv9ET9OKBtyU3Ch5+RztStOUkhswepHv4ZC3MvEKc=",
    "h1:LIbucvxGtgaE+UnyaQhq+OgbO4Gwb6JXJK2EvPKx2Lc=",
    "h1:hPiyFGluON+0/XcTKTSZKwBNzYs5TwEQ5NEZzhDvHH4=",
    "h1:oaZAXF89YQJfkDMfd/XYaILddExj0G+6x82edEpt8fU=",
    "zh:0320a50405093d7c24df1a71711434b1ea2fbfc0ac649e824081976c2d85b196",
    "zh:10cf7455074adca77ce92213658e245fa5de7b3509e93399c85eb7efceb77d02",
    "zh:31edb6dbcd1bd3ffa473a0dce5d1af5314769387b87d41352cba8ee5825b03f5",
    "zh:4c826f301e33258939ca7761e8056c4e4929136e1a6c9e38d443dba9024ea2b8",
    "zh:98207b293a02b72dd50defdae53173234782d8828dc34c3650ad795fc5d09c01",
    "zh:c30ebf3b619aba0a66d3ebe3b014df84339ebb92e24265ff6d044b00b102d582",
    "zh:c4c32d25fd9fdc16d76f57ac8595a36397e6345882ca895c31e71b3d4896cd4a",
    "zh:d9ebd39077458524f6d8aba3b82d2c3a2c22ea85b22d6153fac1977fec91acfd",
    "zh:e1e9b306591bd160cc370614d3a5da2b37c35c4e9e75330cd2e2f929f34a0890",
    "zh:f7db01252ba1314059d9929a9f5708b6cdccf74c0a271518e88b3f93c68cdda6",
    "zh:ffb8581f1bde5621677c132df822663d55956353d052b6ead51b3e11857c10e1",
  ]
}
